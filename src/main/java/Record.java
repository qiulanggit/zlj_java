import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.Gson;
import org.junit.Test;

import java.lang.reflect.Array;

public class Record {
    /**
     * java常用测试笔记
     *
     * @date 2020年12月18
     */

    public static void main(String[] args){

        String s = "{'Cookie':'xxx'}";
        JSONObject josns = JSON.parseObject(s);
        System.out.println(josns);
//        String key = josns.getString("Cookie");


    }


    /**
     * 获取数据类型
     */
    public String getType(Object test) {
        return test.getClass().getName().toString();
    }


    /**
     * 方法重载
     *
     * 匹配到 对应入参自动调用
     */
    public static void requestParams(String url, String headers) {
        String reparams = null;
    }

    public static void requstParams(String url, String reparams) {
        String headers = null;
    }

    public static void requestParams(String url) {
        String headers = null;
        String reparams = null;

    }


    /**
     * 加强for循环
     * for(ElementType element:arrayName){};
     */
    @Test
    public void test() {
        int[] a = {1, 2, 3, 4, 5, 56, 6,};
        for (int i : a) {
            System.out.println(i);
        }

    }



    /**
     * StringBuffer    可变字符串 线程安全 多线程操作字符串
     * StringBuilder   可变字符串 线程不安全 单线程操作
     * String 不可变字符串 每次对字符串擦操作都会新生成一个对象
     * insert 根据位置插入数据
     * append 后面插入数据
     * delete 删除数据
     */
    @Test
    public void addString() {
        String str1 = "{'xxx':'222','www':'333'}";
        StringBuffer str2 = new StringBuffer(str1);
        str2.insert(3, "55555");
        System.out.println(str2);
        str2.append("6666");
        str2.delete(3,7);
        System.out.println(str2);
    }
}